package mapwriter.gui;

import mapwriter.Mw;
import mapwriter.config.WorldConfig;
import mapwriter.map.MapView;
import net.minecraft.client.gui.Screen;
import net.minecraft.client.resource.language.I18n;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
@Environment(EnvType.CLIENT)
public class MwGuiDimensionDialog extends MwGuiTextDialog
{

	final Mw mw;
	final MapView mapView;
	final int dimension;

	public MwGuiDimensionDialog(Screen parentScreen, Mw mw, MapView mapView, int dimension)
	{
		super(parentScreen, I18n.translate("mw.gui.mwguidimensiondialog.title") + ":", Integer.toString(dimension), I18n.translate("mw.gui.mwguidimensiondialog.error"));
		this.mw = mw;
		this.mapView = mapView;
		this.dimension = dimension;
	}

	@Override
	public boolean submit()
	{
		boolean done = false;
		int dimension = this.getInputAsInt();
		if (this.inputValid)
		{
			this.mapView.setDimensionAndAdjustZoom(dimension);
			this.mw.miniMap.view.setDimension(dimension);
			WorldConfig.getInstance().addDimension(dimension);
			done = true;
		}
		return done;
	}
}
